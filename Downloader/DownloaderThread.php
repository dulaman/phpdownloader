<?php

/**
 * @package Downloader
 * @author Adam Dulewicz
 */
namespace Downloader;

use Thread as Thread;

class DownloaderThread extends Thread {

    private $workerId, $url, $path;

    public function __construct(int $id, string $url, string $path) {
        $this->workerId = $id;
        $this->url = $url;
        $this->path = $path;
    }

    public function run() {
        echo "Wątek nr {$this->workerId}" . PHP_EOL .
            "URL: {$this->url}" . PHP_EOL;
        $this->simpleDownloadFile();
    }

    private function simpleDownloadFile() {
        set_time_limit(0);
        //This is the file where we save the information
        $fp = fopen ($this->path . '/' . urlencode($this->url) . '_' . $this->workerId . '.tmp', 'w+');
        //Here is the file we are downloading, replace spaces with %20
        $ch = curl_init(str_replace(" ","%20",$this->url));
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        // write curl response to file
        curl_setopt($ch, CURLOPT_FILE, $fp); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        // get curl response
        curl_exec($ch); 
        curl_close($ch);
        fclose($fp);
    }
}
