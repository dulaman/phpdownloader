<?php

/**
 * @author Adam Dulewicz
 */
function __autoload(string $class_name) {
    $path = preg_replace('/\\\/', '/', $class_name) . '.php';
    if (file_exists($path)) include $path;
}

if (PHP_SAPI !== "cli") exit;

\Main\Main::main($argv, dirname(__FILE__));
