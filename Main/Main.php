<?php

/**
 * @package Main
 * @author Adam Dulewicz
 */
namespace Main;

use Downloader\DownloaderThread;

class Main {

    public static function main(array $args = [], string $path) {

        if (count($args)===1) {
            echo "Nie dodałeś żadnych linków";
            return ;
        }
        if ($args[1]==="--help"||$args[1]==="--h") {
            //TODO: Help info
            return ;
        }

        $workers = [];

        // Initialize and start the threads
        foreach (range(1, count($args)-1) as $i) {
            $workers[$i] = new DownloaderThread($i, $args[$i], $path);
            $workers[$i]->start();
        }

        // Let the threads come back
        foreach (range(1, count($args)-1) as $i) {
            $workers[$i]->join();
        }

    }

}
